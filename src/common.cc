#include "common.h"
/*
std::string HashValue(cv::Mat &src) {  
     std::string rst(64,'\0');  
     cv::Mat img;  
     if(src.channels()==3) {
        cvtColor(src,img,CV_BGR2GRAY);  
     } else {
        img=src.clone();  
     }  
        
       /*第一步，缩小尺寸。 
         将图片缩小到8x8的尺寸，总共64个像素,去除图片的细节
    cv::resize(img,img,Size(8,8));  
    /* 第二步，简化色彩(Color Reduce)。 
       将缩小后的图片，转为64级灰度。*

    uchar *pData;  
    for(int i=0;i<img.rows;i++) {  
        pData = img.ptr<uchar>(i);  
        for(int j=0;j<img.cols;j++) {  
            pData[j]=pData[j]/4;            
        }  
    }  

    /* 第三步，计算平均值。 
       计算所有64个像素的灰度平均值。 
    int average = mean(img).val[0];  

    /* 第四步，比较像素的灰度。 
     将每个像素的灰度，与平均值进行比较。大于或等于平均值记为1,小于平均值记为0*
    cv::Mat mask = (img>=(uchar)average);  

        /* 第五步，计算哈希值。*
    int index = 0;  
    for(int i=0;i<mask.rows;i++) {  
        pData = mask.ptr<uchar>(i);  
        for(int j=0;j<mask.cols;j++) {  
            if(pData[j]==0)  
                rst[index++]='0';  
            else  
                rst[index++]='1';  
        }  
    }  
    return rst;  
 }
*/
std::string pHashValue(cv::Mat &src) {  
     cv::Mat img ,dst;
     std::string rst(64,'\0');
     double dIdex[64];
     double mean = 0.0;
     int k = 0;
     if(src.channels()==3) {
         cv::cvtColor(src,src,CV_BGR2GRAY);  
         img = cv::Mat_<double>(src);
     } else {
         img = cv::Mat_<double>(src);
     }
     cv::resize(img, img, cv::Size(8,8));
     cv::dct(img, dst);
     for (int i = 0; i < 8; ++i) {
         for (int j = 0; j < 8; ++j) {
             dIdex[k] = dst.at<double>(i, j);  
             mean += dst.at<double>(i, j)/64;  
             ++k;
         }
     }
    for (int i =0;i<64;++i) {
        if (dIdex[i]>=mean) {
            rst[i]='1';
        } else {  
            rst[i]='0';  
        }
    }
     return rst;  
 }

 //汉明距离计算  
 int HanmingDistance(std::string &str1, std::string &str2) {  
    if((str1.size()!=64)||(str2.size()!=64))  
        return -1;  
    int difference = 0;  
    for(int i=0;i<64;i++) {  
        if(str1[i]!=str2[i])  
            difference++;  
    }  
    return difference;  
 }  