const compareImage = require('../lib/index');

try {
    let rv = -1;
    rv = compareImage.compareWithPHA("./files/test1.bmp", "./files/test3.bmp");
    console.log("pha test1.bmp compared whie test3.bmp =>", rv);
    rv = compareImage.compareWithPHA("./files/test1.bmp", "./files/test4.bmp");
    console.log("pha test1.bmp compared whie test4.bmp =>", rv);
} catch (e) {
    console.log("pha compare failed");
}

try {
    let rv = 0;
    rv = compareImage.compareWithArray("./files/test1.bmp", "./files/test3.bmp");
    console.log("array test1.bmp compared whie test3.bmp =>", rv);
    rv = compareImage.compareWithArray("./files/test1.bmp", "./files/test2.bmp");
    console.log("array test1.bmp compared whie test2.bmp =>", rv);
} catch (e) {
    console.log("array compare failed");
}
